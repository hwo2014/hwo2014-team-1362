using System;
using System.IO;
using System.Net.Sockets;
using System.Linq;
using Newtonsoft.Json;

public class Bot
{
    double throttle = 0.1;
    int lastPieceIndex = 0;
    int nextPieceId = 0;
    int currentPieceId = 0;
    StreamWriter sw = null;
    int switchMap = 0;
    int switchCount = 0;

    public static void Main(string[] args)
    {
        string host = args[0];
        int port = int.Parse(args[1]);
        string botName = args[2];
        string botKey = args[3];

        Console.WriteLine("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        using (TcpClient client = new TcpClient(host, port))
        {
            Console.WriteLine("Connected");
            NetworkStream stream = client.GetStream();
            StreamReader reader = new StreamReader(stream);
            StreamWriter writer = new StreamWriter(stream) { AutoFlush = true };

            new Bot(reader, writer, new Join(botName, botKey));
        }
    }

    private StreamWriter writer;

    Bot(StreamReader reader, StreamWriter writer, Join join)
    {
        this.writer = writer;
        string line;
        Random r = new Random((int)DateTime.Now.Ticks);

        if (File.Exists("race.log"))
            File.Delete("race.log");

        sw = new StreamWriter("race.log");
        sw.AutoFlush = true;
        YourCar car = null;
        RaceObject ra = null;

        double speed = 0;
        double lastPosition = 0;
        int currentLane;
        bool pauseOutput = false;


        send(join);

        while ((line = reader.ReadLine()) != null)
        {
            MsgWrapper msg = JsonConvert.DeserializeObject<MsgWrapper>(line);
            switch (msg.msgType)
            {
                case "carPositions":
                    {
                        CarPositionObject cp = JsonConvert.DeserializeObject<CarPositionObject>(line);
                        Datum d = (from n in cp.data where n.id.color == car.color select n).FirstOrDefault();

                        currentLane = d.piecePosition.lane.startLaneIndex;
                        currentPieceId = d.piecePosition.pieceIndex;

                        lastPieceIndex = d.piecePosition.pieceIndex - 1;
                        if (lastPieceIndex < 0)
                            lastPieceIndex = ra.race.track.pieces.Count - 1;

                        if (lastPosition == 0)
                        {
                            lastPosition = d.piecePosition.inPieceDistance;
                        }
                        else
                        {
                            if (d.piecePosition.inPieceDistance < lastPosition) // moved to new track piece
                            {
                                speed = (100 - (double)(ra.race.track.pieces[lastPieceIndex].length == 0 ? ra.race.track.pieces[lastPieceIndex].radius.Value : ra.race.track.pieces[lastPieceIndex].length)) + d.piecePosition.inPieceDistance;
                            }
                            else
                            {
                                speed = d.piecePosition.inPieceDistance - lastPosition;
                            }
                            lastPosition = d.piecePosition.inPieceDistance;
                        }

                        Piece currentPiece = ra.race.track.pieces[d.piecePosition.pieceIndex];
                        Piece nextPiece = null;
                        if ((d.piecePosition.pieceIndex + 1) >= ra.race.track.pieces.Count)
                        {
                            nextPiece = ra.race.track.pieces[0];
                            nextPieceId = 0;
                        }
                        else
                        {
                            nextPiece = ra.race.track.pieces[d.piecePosition.pieceIndex + 1];
                            nextPieceId = d.piecePosition.pieceIndex + 1;
                        }

                        if (!pauseOutput)
                        {
                            Console.Clear();
                            Console.WriteLine(line);

                            Console.WriteLine("Speed: {0}", speed);
                            Console.WriteLine("Lane: {0}", d.piecePosition.lane.startLaneIndex);
                            Console.WriteLine("Throttle: {0}", throttle);
                            Console.WriteLine("Angle: {0}", d.angle);
                            Console.WriteLine("Lap:{0}", d.piecePosition.lap);
                            Console.WriteLine("Previous Piece ID:{0}", lastPieceIndex);
                            Console.WriteLine("Track Piece:{0}", d.piecePosition.pieceIndex);
                            Console.WriteLine("Next Piece:{0}", nextPieceId);
                            Console.WriteLine("Position In Piece:{0}", d.piecePosition.inPieceDistance);
                            //Console.WriteLine("Tick:{0}", d.)
                            sw.WriteLine("Speed: {0}, Lane: {1}, Throttle: {2}, Angle: {3}, Lap:{4}, Track Piece:{5}, Position In Piece:{6}",
                                speed, currentLane, throttle, d.angle, d.piecePosition.lap, d.piecePosition.pieceIndex, d.piecePosition.inPieceDistance);
                        }

                        ShouldAdjustSpeed(currentPiece, nextPiece, d, ra);


                        if (ShouldSwitch(currentPiece, nextPiece, d, ra))
                            break;

                        send(new Ping());
                    } break;
                case "join":
                    Console.WriteLine("Joined");
                    send(new Ping());
                    break;
                case "gameInit":
                    Console.WriteLine("Race init");
                    ra = JsonConvert.DeserializeObject<RaceObject>(msg.data.ToString());
                    sw.WriteLine(msg.data.ToString());
                    send(new Ping());
                    break;
                case "gameEnd":
                    Console.WriteLine("Race ended");
                    send(new Ping());
                    break;
                case "gameStart":
                    Console.WriteLine("Race starts");
                    send(new Ping());
                    break;
                case "yourCar":
                    car = JsonConvert.DeserializeObject<YourCar>(msg.data.ToString());
                    break;
                case "lapFinished":
                    Console.WriteLine("Lap Finished");
                    switchMap = 0;
                    switchCount = 0;
                    break;
                case "crash":
                    sw.WriteLine("Smashed the car again!");
                    Console.WriteLine("Smashed the car again!");
                    pauseOutput = true;
                    break;
                case "spawn":
                    sw.WriteLine("Car back on the track!");
                    Console.WriteLine("Car back on the track!");
                    pauseOutput = false;
                    break;
                default:
                    send(new Ping());
                    Console.WriteLine(line);
                    break;
            }
        }
        sw.Close();
    }

    private bool ShouldAdjustSpeed(Piece currentPiece, Piece nextPiece, Datum d, RaceObject ra)
    {
        send(new Throttle(0.5));
        return true;

        // straight into another straight
        if (currentPiece.length > 0 && nextPiece.length > 0)
        {
            throttle = 1.0;
            send(new Throttle(throttle));
            return true;
        }


        if (currentPiece.length > 0 && nextPiece.length == 0 && d.piecePosition.inPieceDistance < 75)
        {
            throttle = 0.2;
            Math.Max(0.1, throttle);
            send(new Throttle(throttle));
            return true;
        }

        if (currentPiece.length == 0 && nextPiece.length > 0 && d.piecePosition.inPieceDistance > 50)
        {
            throttle += 0.3;
            send(new Throttle(throttle));
            return true;
        }

        if (nextPiece.length == 0)
        {
            throttle -= 0.4;
            throttle = Math.Max(0.1, throttle);
            send(new Throttle(throttle));
            return true;
        }

        return false;
    }

    private bool ShouldSwitch(Piece currentPiece, Piece nextPiece, Datum currentState, RaceObject raceData)
    {
        //TODO: Determine inside line for the track
        if (!nextPiece.@switch.HasValue || !nextPiece.@switch.Value)
            return false;

        if((switchMap & (1 << switchCount)) != 0)
        {
            sw.WriteLine("Handled this switch before this lap");
            sw.WriteLine("Switchmap: {0}", Convert.ToString(switchMap, 2));
            return false;
        }

        switchCount++;
        switchMap |= 1 << switchCount;

        sw.WriteLine("Next piece has a switch");

        int lnextPieceId = 0;
        if ((currentState.piecePosition.pieceIndex + 1) >= raceData.race.track.pieces.Count)
        {
            lnextPieceId = 0;
        }
        else
        {
            lnextPieceId = currentState.piecePosition.pieceIndex + 1;
        }

        Piece tempPiece = raceData.race.track.pieces[lnextPieceId];

        // We are on a piece of track that has a switch
        // If the angle of the next track piece is  positive it is a right hand turn, negative left hand
        if ((nextPiece.angle > 0 || tempPiece.angle > 0) && currentState.piecePosition.lane.startLaneIndex != raceData.race.track.lanes.Count - 1)
        {
            
            send(new SwitchRight());
            return true;
        }

        if ((nextPiece.angle < 0 || tempPiece.angle < 0) && currentState.piecePosition.lane.startLaneIndex != 0)
        {
            send(new SwitchLeft());
            return true;
        }

        return false;
    }

    private void send(SendMsg msg)
    {
        writer.WriteLine(msg.ToJson());
    }
}

class MsgWrapper
{
    public string msgType;
    public Object data;

    public MsgWrapper(string msgType, Object data)
    {
        this.msgType = msgType;
        this.data = data;
    }
}

abstract class SendMsg
{
    public string ToJson()
    {
        return JsonConvert.SerializeObject(new MsgWrapper(this.MsgType(), this.MsgData()));
    }
    protected virtual Object MsgData()
    {
        return this;
    }

    protected abstract string MsgType();
}

class Join : SendMsg
{
    public string name;
    public string key;
    public string color;

    public Join(string name, string key)
    {
        this.name = name;
        this.key = key;
        this.color = "green";
    }

    protected override string MsgType()
    {
        return "join";
    }
}

class Ping : SendMsg
{
    protected override string MsgType()
    {
        return "ping";
    }
}

class Throttle : SendMsg
{
    public double value;

    public Throttle(double value)
    {
        this.value = value;
    }

    protected override Object MsgData()
    {
        return Math.Max((double)this.value, 0.2);
    }

    protected override string MsgType()
    {
        return "throttle";
    }
}

class SwitchRight : SendMsg
{
    protected override Object MsgData()
    {
        return "Right";
    }

    protected override string MsgType()
    {
        return "switchLane";
    }
}

class SwitchLeft : SendMsg
{
    protected override Object MsgData()
    {
        return "Left";
    }

    protected override string MsgType()
    {
        return "switchLane";
    }
}

class YourCar
{
    public string name { get; set; }
    public string color { get; set; }
}
