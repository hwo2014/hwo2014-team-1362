﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class Id
{
    public string name { get; set; }
    public string color { get; set; }
}

public class Lane
{
    public int startLaneIndex { get; set; }
    public int endLaneIndex { get; set; }
    public int distanceFromCenter { get; set; }
    public int index { get; set; }
}

public class PiecePosition
{
    public int pieceIndex { get; set; }
    public double inPieceDistance { get; set; }
    public Lane lane { get; set; }
    public int lap { get; set; }
}

public class Datum
{
    public Id id { get; set; }
    public double angle { get; set; }
    public PiecePosition piecePosition { get; set; }
}

public class CarPositionObject
{
    public string msgType { get; set; }
    public List<Datum> data { get; set; }
    public string gameId { get; set; }
    public int gameTick { get; set; }
}
